Welcome
============

This tip calculator is meant to be a "get your feet wet" project for getting an app into the app store, as well as getting more familiar with iOS.


Rights
------------
You're free to use this software for non-commercial usage. Please do not submit it to any app store.


App Store
------------
Check out this app in the Apple App Store: https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=871585600&mt=8

Images
------------
Here is what version 1.1 of the app looks like:

![Main Tab](https://i.imgur.com/8TBlsckl.png "Main Tab")
![Calculations Tab](https://i.imgur.com/sAELZHOl.png "Calculations Tab")
![About Tab](http://i.imgur.com/YAHw25El.png "About Tab")