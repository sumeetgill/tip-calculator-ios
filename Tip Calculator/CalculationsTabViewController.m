//
//  CalculationsViewController.m
//  Tip Calculator
//
//  Created by Sumeet Gill on 2014-04-25.
//  Copyright (c) 2014 Sumeet Gill. All rights reserved.
//

#import "CalculationsTabViewController.h"


@interface CalculationsTabViewController ()

@end

@implementation CalculationsTabViewController
@synthesize calculationsDisplayArray;
@synthesize tableView;
@synthesize bannerView;

- (void)viewDidLoad
{
    [super viewDidLoad];

    calculationsDisplayArray = calculationsDisplayArray ?: // checks if null
    UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? // checks device type
    [[NSMutableArray alloc] initWithObjects:@"Please go to the Main Tab and", @" enter in values.",  @"", @"To save time, enter in values and ", @"click 'Save' to save default values.", @"The values will appear when", @"the app starts up", nil]
    : [[NSMutableArray alloc] initWithObjects:@"Please go to the Main Tab and enter in values.", @"", @"To save time, enter in values and click 'Save' to save default values.", @"The values will appear when the app starts up.", nil];
    
    // Changes the ad location depending on device
    bannerView = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? // Checks if iPhone
    [[GADBannerView alloc] initWithFrame:CGRectMake(0.0, 420.00 - GAD_SIZE_320x50.height, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height)]
    :[[GADBannerView alloc] initWithFrame:CGRectMake(250.00, 950.00 - GAD_SIZE_320x50.height, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height)];
    
    /* Create the ad banner of the standard size at the bottom of the screen */
    bannerView.adUnitID = ADMOB_AD_KEY; //Specify the ad unit ID, removed from commit
    
    //Let the runtime know which UIViewController to restore after taking the user wherever the ad goes and add it to the view hierarchy
    bannerView.rootViewController = self;
    
    //initiate a generic request to load it with an ad
    [bannerView loadRequest:[GADRequest request]];
    
    [self.view addSubview:bannerView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reloadTable{
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [calculationsDisplayArray count]; // Return the number of rows in the section.
}

//Configures the cell to display what is contained in the array
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"calculationsCell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.textLabel.text = [calculationsDisplayArray objectAtIndex:indexPath.row];
    
    return cell;
}


@end
