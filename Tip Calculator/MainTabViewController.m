//
//  FirstViewController.m
//  Tip Calculator
//
//  Created by Sumeet Gill on 2014-04-24.
//  Copyright (c) 2014 Sumeet Gill. All rights reserved.
//

#import "MainTabViewController.h"
#import "CalculationsTabViewController.h"

@interface MainTabViewController ()


@end
@implementation MainTabViewController

@synthesize txtSubtotal;
@synthesize txtTax;
@synthesize txtTipPercentage;
@synthesize txtTipAmount;
@synthesize txtBillSplit;
@synthesize btnCalculate;
@synthesize btnClear;


/**
 *  Gets the default values and loads them into the textViews after the view has loaded
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *tipDefaults = [NSUserDefaults standardUserDefaults];
    txtSubtotal.text = [tipDefaults objectForKey:@"subtotal"];
    txtTax.text = [tipDefaults objectForKey:@"tax"];
    txtTipPercentage.text = [tipDefaults objectForKey:@"tipPercentage"];
    txtTipAmount.text = [tipDefaults objectForKey:@"tipAmount"];
    txtBillSplit.text = [tipDefaults objectForKey:@"billSplit"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Keyboard Methods

/**
 *  Will hide the keyboard when any of the text boxes lose focus, to allow users to continue inputting values
 *
 *  @param touches
 *  @param event
 */
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
//    UITouch *touch = [[event allTouches] anyObject];
    
    [self.view endEditing:YES]; // Easier than resigning first responder for each textbox
}

#pragma Button Methods

/**
 *  Lets the user save default values for their calculations, items such as tax will not vary much, so it will save the user time
 *
 *  @param sender btnSave is pressed by the user to save their default values
 */
-(IBAction)btnSaveDefaultsPressed:(id)sender{
    
    NSUserDefaults *tipDefaults = [NSUserDefaults standardUserDefaults];
    
    [tipDefaults setObject:txtSubtotal.text forKey:@"subtotal"];
    [tipDefaults setObject:txtTax.text forKey:@"tax"];
    [tipDefaults setObject:txtTipPercentage.text forKey:@"tipPercentage"];
    [tipDefaults setObject:txtTipAmount.text forKey:@"tipAmount"];
    [tipDefaults setObject:txtBillSplit.text forKey:@"billSplit"];
    
    [tipDefaults synchronize];
    
    // Give the user some verification that their defaults were saved
    UIAlertView *saveAlert =[[UIAlertView alloc] initWithTitle:@"Success" message:@"Your default values have been saved." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [saveAlert show];
}

/**
 *  Starts the calculations, beginning with validation and ending with displaying results
 *
 *  @param sender btnCalculate is pressed by the user
 */
-(IBAction)btnCalculatePressed:(id)sender{
    
    NSString *isDataValidString = [self isDataValid];
    if ([isDataValidString isEqual: @"Valid"]) {
        [self calculateTip];
    } else { //display the error message that was returned from the method, in the form of an alert
        UIAlertView *dataAlert =[[UIAlertView alloc] initWithTitle:@"Error" message:isDataValidString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [dataAlert show];
    }
}


/**
 *  This method is to clear the text fields when the user clicks the clear button ^SG
 *
 *  @param sender btnClear is pressed by the user
 */
-(IBAction)btnClearPressed:(id)sender{
    txtSubtotal.text = @"";
    txtTax.text = @"";
    txtTipPercentage.text = @"";
    txtTipAmount.text = @"";
    txtBillSplit.text = @"";
}



#pragma Tip Calculations

/**
 *  Method is used to make sure the correct fields are not blank
 *
 *  @return Whether or not the user inputted numbers are valid for calculation
 */
-(NSString *)isDataValid{
    
    if ([txtSubtotal.text length] == 0) {
        [txtSubtotal becomeFirstResponder]; //Set the focus to subtotal if the user forgot, for better ease of use
        return @"Subtotal cannot be blank.";
    } else if ([txtTipPercentage.text length] == 0 && [txtTipAmount.text length] == 0){
        [txtTipPercentage becomeFirstResponder]; //Setting it as percentage because it is more likely to be the one being used
        return @"Please enter either a value for Tip Percentage or Tip Amount.";
    } else if ([txtTipPercentage.text length] > 0 && [txtTipAmount.text length] > 0){
        [txtTipAmount becomeFirstResponder];
        return @"Please enter a value in either Tip Percentage OR Tip Amount. Both cannot be filled at the same time.";
    }

    return @"Valid";
}



/**
 *  calculateTip will be used to calculate either the tip percentage given or tip amount to be given, depending on the user input
 *  This method will be moved to it's own class file instead of being in the viewcontroller at some point in the future
 */
-(void)calculateTip {
    double txtSubtotalValue = [txtSubtotal.text doubleValue];
    double txtTaxValue = [txtTax.text doubleValue];
    double txtBillSplitValue = [txtBillSplit.text doubleValue];
    
    double tipTotal = [txtTipAmount.text doubleValue];
    double tipPercentageGiven = [txtTipPercentage.text doubleValue];
    
    double grandTotal = 0;
    double taxTotal = 0;
    
    double costPerPersonTotal = 0;
    double tipPerPersonTotal = 0;
    
    //If the value is 0 for billsplit, set it to one to allow the calculations to complete anyways.
    if (txtBillSplitValue == 0) {
        txtBillSplitValue = 1;
    }
    
    //If the value is 0 for tax, then the total will be assigned 0, otherwise it will be calculated
    taxTotal = txtSubtotalValue * (txtTaxValue / 100); //Divide by 100 to get decimal
    
    
    //Calculating the tip total based on percentage given
    if ([txtTipPercentage.text length] > 0) {
        tipTotal = txtSubtotalValue * (tipPercentageGiven / 100); //Divide by 100 to get decimal
        grandTotal = txtSubtotalValue + taxTotal + tipTotal;
    } else if ([txtTipAmount.text length] > 0){ //Calculating tip percentage based on amount given
        tipPercentageGiven = (tipTotal / txtSubtotalValue) * 100; // Multiply by 100 to move decimal points for output
        grandTotal = txtSubtotalValue + tipTotal + taxTotal;
    }
    
    tipPerPersonTotal = tipTotal / txtBillSplitValue;
    costPerPersonTotal = grandTotal / txtBillSplitValue;
    
    [self storeCalculationsDataStartingWithSubtotal:txtSubtotalValue taxTotal:taxTotal tipTotal:tipTotal tipPerPerson:tipPerPersonTotal tipPercentage:tipPercentageGiven costPerPerson:costPerPersonTotal grandTotal:grandTotal];
}

/**
 *  Will store all the calculation data into an array and send it to another view controller to be displayed
 *  Sidenote: made a separate method for storing to allow for easier maintenence later down the line
 */
-(void)storeCalculationsDataStartingWithSubtotal:(double)subTotal taxTotal:(double)taxTotal tipTotal:(double)tipTotal tipPerPerson:(double)tipPerPersonTotal tipPercentage:(double)tipPercentageGiven costPerPerson:(double)costPerPersonTotal grandTotal:(double)grandTotal {
    //Add values into an array for displaying on the UITABLE later
    NSMutableArray *calculationResultArray = [[NSMutableArray alloc] init];
    
    //Using .2f to limit the double to two digits only -- just a heads up
    [calculationResultArray  addObject:[NSString stringWithFormat:@"Subtotal: $%.2f",subTotal]];
    [calculationResultArray addObject:[NSString stringWithFormat:@"Tax Amount: $%.2f", taxTotal]];
    [calculationResultArray addObject:[NSString stringWithFormat:@"Tip Percentage: %.2f%@", tipPercentageGiven, @"%"]];
    [calculationResultArray addObject:[NSString stringWithFormat:@"Tip Amount: $%.2f", tipTotal]];
    
    if (tipTotal != tipPerPersonTotal) { // this is to not display per person values if there's only one person
        [calculationResultArray addObject:[NSString stringWithFormat:@"Tip Amount (Per Person): $%.2f", tipPerPersonTotal]];
        [calculationResultArray addObject:[NSString stringWithFormat:@"Total (Per Person): $%.2f", costPerPersonTotal]];
    }
    
    [calculationResultArray addObject:[NSString stringWithFormat:@"Grand Total: $%.2f", grandTotal]];
    
    [self displayCalculationsView:calculationResultArray];
}

/**
 *  Changes the screen to the calculations tab and will add a transition effect
 *
 *  @param array Holds all the values that have been calculated based on user input. It is passed to the other view controller.
 */
-(void) displayCalculationsView:(NSMutableArray *)array{
    CalculationsTabViewController *calculationsVC = [self.tabBarController. viewControllers objectAtIndex:1];
    calculationsVC.calculationsDisplayArray = array;
    
    [self.tabBarController setSelectedIndex:1];
    [calculationsVC reloadTable];
}


@end
