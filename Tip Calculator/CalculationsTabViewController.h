//
//  CalculationsViewController.h
//  Tip Calculator
//
//  Created by Sumeet Gill on 2014-04-25.
//  Copyright (c) 2014 Sumeet Gill. All rights reserved.
//

#import <UIKit/UIKit.h>

@import GoogleMobileAds;

@interface CalculationsTabViewController : UIViewController{
    NSMutableArray *calculationsDisplayArray;
    UITableView *tableView;
    GADBannerView *bannerView;
}

@property(nonatomic,strong) NSMutableArray *calculationsDisplayArray;
@property(strong, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic,strong) IBOutlet GADBannerView *bannerView;


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;


-(void)reloadTable;
@end
