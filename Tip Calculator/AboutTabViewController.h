//
//  AboutViewController.h
//  Tip Calculator
//
//  Created by Sumeet Gill on 2014-04-24.
//  Copyright (c) 2014 Sumeet Gill. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutTabViewController : UIViewController{
    UIPickerView *pickerView;
    UITextView *aboutTextView;
    NSArray *pickerTitleArray;
}

@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;

@property (strong, nonatomic) IBOutlet UITextView *aboutTextView;

@end
