//
//  FirstViewController.h
//  Tip Calculator
//
//  Created by Sumeet Gill on 2014-04-24.
//  Copyright (c) 2014 Sumeet Gill. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTabViewController : UIViewController{
    IBOutlet UITextField *txtSubtotal;
    IBOutlet UITextField *txtTax;
    IBOutlet UITextField *txtTipPercentage;
    IBOutlet UITextField *txtTipAmount;
    IBOutlet UITextField *txtBillSplit;
    
    IBOutlet UIButton *btnCalculate;
    IBOutlet UIButton *btnClear;
}
@property(nonatomic,strong) UINavigationController *navigationController;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnSaveDefaults;

@property (strong, nonatomic) IBOutlet UIButton *btnSaveDefaultsiPad;

@property (strong,strong) IBOutlet UITextField *txtSubtotal;
@property (strong,strong) IBOutlet UITextField *txtTax;
@property (strong,strong) IBOutlet UITextField *txtTipPercentage;
@property (strong,strong) IBOutlet UITextField *txtTipAmount;
@property (strong,strong) IBOutlet UITextField *txtBillSplit;

@property (strong,strong) IBOutlet UIButton *btnCalculate;
@property (strong,strong) IBOutlet UIButton *btnClear;

-(IBAction)btnCalculatePressed:(id)sender;
-(IBAction)btnClearPressed:(id)sender;

-(IBAction)btnSaveDefaultsPressed:(id)sender;

-(NSString *)isDataValid;


@end
