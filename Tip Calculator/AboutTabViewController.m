//
//  AboutViewController.m
//  Tip Calculator
//
//  Created by Sumeet Gill on 2014-04-24.
//  Copyright (c) 2014 Sumeet Gill. All rights reserved.
//

#import "AboutTabViewController.h"

@interface AboutTabViewController ()

@end

@implementation AboutTabViewController
@synthesize pickerView;
@synthesize aboutTextView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/**
 *  Creates an array to later populate the pickerView
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    pickerTitleArray = @[@"Credits", @"App Version", @"Contact Developer", @"Appreciation"];
}


/**
 *  Sets the Credits as the default option and have the textView populated with correct info
 *
 *  @param animated Bool to animate or not
 */
-(void)viewDidAppear:(BOOL)animated{
    [pickerView selectRow:0 inComponent:0 animated:YES];
    [self pickerView:pickerView didSelectRow:0 inComponent:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Picker Delegates

/**
 *  Tells the aboutTabTextView what to display based on the user's choice in pickerView
 *
 *  @param pickerView A control on the page that allows users to select from multiple choices
 *  @param row        The option that the user has selected
 *  @param component
 */
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    //Handle the selection for pickerView
    switch (row) {
        case 0: //Credits
            aboutTextView.text = @"This app was created by Sumeet Gill. \n \n App icon is courtesy of RainDropMemory via: \n http://www.raindropmemory.deviantart.com \n \n Tab bar icons are courtesy of Icons8 via: \n http://icons8.com";
            break;
        case 1: // App Version
            aboutTextView.text = [NSString stringWithFormat:@"Version: %@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
            break;
        case 2: // Contact Developer
            aboutTextView.text = @"Comments? Questions? Contact me at: sumeet.gill@icloud.com";
            break;
        case 3: //Appreciation
            aboutTextView.text = @"Thank you for downloading my app. I hope you enjoy it! \n \n Please rate it in the App Store!";
            break;
        default:
            break;
    }
    
    aboutTextView.textAlignment = NSTextAlignmentCenter;
}


/**
 *  Tells the picker how many rows are available for a given component
 *
 *  @param pickerView A control on the page that allows users to select from multiple choices
 *  @param component
 *
 *  @return The number of elements in the array that was created for the menu
 */
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [pickerTitleArray count];
}

/**
 *  Tells the picker how many components it will have
 *
 *  @param pickerView
 *
 *  @return There is only one component, so 1 is returned
 */
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

/**
 *  Tells the picker the title for a given component, based on what is in the picker title array
 *
 *  @param pickerView 
 *  @param row        User selected row
 *  @param component
 *
 *  @return Title of the row, in the array
 */
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return pickerTitleArray[row];
}

/**
 *  Tells the picker the width of each row for a given component
 *
 *  @param pickerView
 *  @param component
 *
 *  @return returns 300, which is the section width
 */
-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return 300;
}

@end
