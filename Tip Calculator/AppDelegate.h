//
//  AppDelegate.h
//  Tip Calculator
//
//  Created by Sumeet Gill on 2014-04-24.
//  Copyright (c) 2014 Sumeet Gill. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
